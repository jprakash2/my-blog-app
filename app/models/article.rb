class Article < ApplicationRecord
  include Visible

  has_many :comments
  has_one_attached :image, dependent: :destroy

  validates :title, presence: true
  validates :body, presence: true, length: { minimum: 10, maximum: 140 }
end
