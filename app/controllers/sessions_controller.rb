class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.where(email: params[:email], password: params[:password]).first
    if user
      session[:user_id] = user.id
      redirect_to root_url, notice: "Welcome back!"
    else
      flash.now[:alert] = "Email or password is invalid"
      render "new"
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url, notice: "Logged out!"
  end
  
end